        Sprite sprite = new Sprite(new Texture("assets/game/pixel.png"));
        sprite.setColor(0f, 1f, 0f, 1f);
        SpriteDrawable spriteDrawable = new SpriteDrawable(sprite);
        spriteDrawable.setMinHeight(200);
        spriteDrawable.setMinWidth(200);
        LabelStyle labelStyle = new LabelStyle();
        labelStyle.background = spriteDrawable;
        labelStyle.font = generator.generateFont(48);
