package de.passau.ieee.woc.sillard.model.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.systems.IntervalEntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

import de.passau.ieee.woc.sillard.model.GoalListener;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.components.Physics;

import java.util.LinkedList;
import java.util.List;

public class PhysicsSystem extends IntervalEntityProcessingSystem implements ContactListener {
    // TODO: make distinction between mobile/desktop
    // 1/45 for mobile, 1/300 for desktop
    private static final float TIME_STEP = 1f / 45f;
    private World physicsWorld;
    private List<GoalListener> goalListeners;
    // Whether there is movement in the physics system
    private boolean systemAlive;

    @SuppressWarnings("unchecked")
    public PhysicsSystem() {
        super(Aspect.getAspectForAll(Physics.class), TIME_STEP);
        goalListeners = new LinkedList<>();
        physicsWorld = new World(Vector2.Zero, false);
        physicsWorld.setContinuousPhysics(true);
        physicsWorld.setContactListener(this);
    }

    public World getPhysicsWorld() {
        return physicsWorld;
    }

    public void setSystemAlive(boolean systemAlive) {
        this.systemAlive = systemAlive;
    }

    public boolean isSystemAlive() {
        return systemAlive;
    }

    public void addGoalListener(GoalListener goalListener) {
        goalListeners.add(goalListener);
    }

    private void handleGoal(String team) {
        for (GoalListener listener : goalListeners) {
            listener.goal(team);
        }
    }

    @Override
    public boolean isPassive() {
        return super.isPassive();
    }

    @Override
    public void setPassive(boolean passive) {
        super.setPassive(passive);
    }

    @Override
    protected void process(Entity entity) {
        if(world.getManager(GroupManager.class).getGroups(entity).contains(ModelConstants.Groups.BALL.toString())) {
            entity.getComponent(Physics.class).applyFriction(0.01f, 0.05f);
        } else {
            entity.getComponent(Physics.class).applyFriction(0.02f, 0.05f);
        }
    }

    @Override
    protected void processEntities(ImmutableBag<Entity> entities) {
        super.processEntities(entities);
        physicsWorld.step(TIME_STEP, 6, 2);

        ImmutableBag<Entity> players = world.getManager(GroupManager.class).getEntities(ModelConstants.Groups.PLAYER.toString());
        ImmutableBag<Entity> balls = world.getManager(GroupManager.class).getEntities(ModelConstants.Groups.BALL.toString());

        systemAlive = false;
        for(int i = 0; i < players.size(); i++) {
            if(players.get(i).getComponent(Physics.class).getBody().getLinearVelocity().len() > 0.1f){
                systemAlive = true;
                break;
            }
        }
        
        if (!systemAlive) {
            // no player is moving anymore, let's check the ball(s) now
            for(int i = 0; i < balls.size(); i++) {
                if(balls.get(i).getComponent(Physics.class).getBody().getLinearVelocity().len() > 0.1f){
                    systemAlive = true;
                    break;
                }
            }
        }
        
        if(!systemAlive) {
            /* All movement has stopped (well, almost at least).
             * Let's completely halt all bodies (players and balls) now.
             */
            for(int i = 0; i < players.size(); i++) {
                players.get(i).getComponent(Physics.class).getBody().getLinearVelocity().mul(0);
            }
            for(int i = 0; i < balls.size(); i++) {
                balls.get(i).getComponent(Physics.class).getBody().getLinearVelocity().mul(0);
            }
        }
    }

    @Override
    protected void inserted(Entity e) {
        e.getComponent(Physics.class).getBody().setUserData(e);
    }

    @Override
    protected void removed(Entity e) {
        physicsWorld.destroyBody(e.getComponent(Physics.class).getBody());
        e.getComponent(Physics.class).getBody().setUserData(null);
        e.getComponent(Physics.class).setBody(null);
    }

    @Override
    public void beginContact(Contact contact) {
        Entity a = (Entity) contact.getFixtureA().getBody().getUserData();
        Entity b = (Entity) contact.getFixtureB().getBody().getUserData();

        ImmutableBag<String> groupsA = world.getManager(GroupManager.class).getGroups(a);
        ImmutableBag<String> groupsB = world.getManager(GroupManager.class).getGroups(b);

        if (groupsA.contains(ModelConstants.Groups.BALL.toString()) && groupsB.contains(ModelConstants.Groups.GOAL.toString())) {
            handleGoal(world.getManager(PlayerManager.class).getPlayer(b));
        } else if(groupsA.contains(ModelConstants.Groups.GOAL.toString()) && groupsB.contains(ModelConstants.Groups.BALL.toString())) {
            handleGoal(world.getManager(PlayerManager.class).getPlayer(a));
        }
    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse  impulse) {
    }
}
