package de.passau.ieee.woc.sillard.model;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.utils.Bag;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import de.passau.ieee.woc.sillard.model.components.Arrow;
import de.passau.ieee.woc.sillard.model.components.Physics;
import de.passau.ieee.woc.sillard.model.systems.AISystem;
import de.passau.ieee.woc.sillard.model.systems.PhysicsSystem;

public class SillardWorld extends World {
    private Bag<Entity> players;
    private Entity ball;

    public SillardWorld() {
        super();
        setManager(new GroupManager());
        setManager(new PlayerManager());
        setSystem(new AISystem(), true);
        setSystem(new PhysicsSystem());
        // TODO: change inital size when there are more the
        players = new Bag<>(15);
    }

    @Override
    public void initialize() {
        super.initialize();
        setupField();
    }

    public Bag<Entity> getPlayers() {
        return players;
    }

    public Entity getBall() {
        return ball;
    }

    private void setupField() {
        // Create goals
        EntityFactory.createGoal(this, ModelConstants.Teams.TeamA.toString(), 0, (ModelConstants.FIELD_HEIGHT - ModelConstants.GOAL_SIZE) / 2);
        EntityFactory.createGoal(this, ModelConstants.Teams.TeamB.toString(), ModelConstants.FIELD_WIDTH, (ModelConstants.FIELD_HEIGHT - ModelConstants.GOAL_SIZE) / 2);

        // Create walls
        EntityFactory.createWall(this, 0, 0, true);
        EntityFactory.createWall(this, 0, ModelConstants.FIELD_HEIGHT, true);
        EntityFactory.createWall(this, 0, 0, false);
        EntityFactory.createWall(this, 0, (ModelConstants.FIELD_HEIGHT + ModelConstants.GOAL_SIZE) / 2, false);
        EntityFactory.createWall(this, ModelConstants.FIELD_WIDTH, 0, false);
        EntityFactory.createWall(this, ModelConstants.FIELD_WIDTH, (ModelConstants.FIELD_HEIGHT + ModelConstants.GOAL_SIZE) / 2, false);
        EntityFactory.createBottomLeftCorner(this);
        EntityFactory.createBottomRightCorner(this);
        EntityFactory.createUpLeftCorner(this);
        EntityFactory.createUpRightCorner(this);
    }

    public void setupPlayers(int playerCount) {
        Vector2[] team = ModelConstants.MeeplePositions.INITIAL_POSITION[playerCount - 2];

        for (int i = 0; i < playerCount; i++) {
            players.add(EntityFactory.createSoccerPlayer(this, ModelConstants.Teams.TeamA.toString(), team[i].x, team[i].y));
            players.add(EntityFactory.createSoccerPlayer(this, ModelConstants.Teams.TeamB.toString(), ModelConstants.FIELD_WIDTH - team[i].x, team[i].y));
        }
        ball = EntityFactory.createBall(this, ModelConstants.FIELD_WIDTH / 2, ModelConstants.FIELD_HEIGHT / 2);
        players.add(ball);
    }

    public void clearPlayers() {
        // TODO: remove physics bodies
        while(!players.isEmpty()) {
            players.removeLast().deleteFromWorld();
        }
    }

    public void resetArrows() {
        ImmutableBag<Entity> ents = getManager(GroupManager.class).getEntities(ModelConstants.Groups.ARROW.toString());
        for(int i = 0; i < ents.size(); i++) {
            FixtureDef fixtureDef = ents.get(i).getComponent(Arrow.class).getFixtureDef();
            ((EdgeShape) fixtureDef.shape).set(Vector2.Zero, Vector2.Zero);
            ents.get(i).getComponent(Arrow.class).setSize(Vector2.Zero);

            // create a new fixture
            ents.get(i).getComponent(Physics.class).getBody().createFixture(fixtureDef);
        }
    }
}
