package de.passau.ieee.woc.sillard.model.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class Arrow extends Component {

    private FixtureDef fixture;
    private Vector2 size;

    public FixtureDef getFixtureDef() {
        return fixture;
    }

    public Arrow(FixtureDef fixture) {
        this.fixture = fixture;
        size = new Vector2();
    }

    public void setSize(Vector2 v) {
        size = v;
    }

    public Vector2 getSize() {
        return size;
    }
}
