package de.passau.ieee.woc.sillard.model.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;

import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.SillardWorld;
import de.passau.ieee.woc.sillard.model.components.AIInput;
import de.passau.ieee.woc.sillard.model.components.Physics;

public class AISystem extends EntityProcessingSystem {
    
    @SuppressWarnings("unchecked")
    public AISystem() {
        super(Aspect.getAspectForAll(AIInput.class));
    }

    @Override
    protected void process(Entity entity) {
        /*Vector2 velocity;
        
        velocity = new Vector2(((SillardWorld) world).getBall().getComponent(Physics.class).getPosition().x - entity.getComponent(Physics.class).getPosition().x, 
                               ((SillardWorld) world).getBall().getComponent(Physics.class).getPosition().y - entity.getComponent(Physics.class).getPosition().y);
        if (velocity.len() < 15) velocity.mul(-5);
        entity.getComponent(AIInput.class).getBody().setLinearVelocity(velocity );*/

        Vector2 posBall = ((SillardWorld) world).getBall().getComponent(Physics.class).getPosition();
        Vector2 posPlayer = entity.getComponent(Physics.class).getPosition();

        entity.getComponent(AIInput.class).getBody().setLinearVelocity(posPlayer.sub(posBall).mul(-1).nor().mul(ModelConstants.MAX_VELOCITY * 5));
    }
    
    @Override
    protected void end() {
        setPassive(true);
        super.end();
    }
    
    @Override
    public void setPassive(boolean passive) {
        super.setPassive(passive);
    }
    
}
