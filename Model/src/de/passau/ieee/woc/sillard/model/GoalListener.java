package de.passau.ieee.woc.sillard.model;

public interface GoalListener {
    void goal(String team);
}
