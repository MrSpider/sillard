package de.passau.ieee.woc.sillard.model.components;

import com.artemis.Component;

public class Sprite extends Component {
    // The name to find the sprite in the main texture atlas
    private String name;
    private float scale;
    private float rotation;
    private boolean customOrigin = false;
    private float originX;
    private float originY;

    public Sprite(String name) {
        this.name = name;
        this.scale = 1f;
        this.rotation = 0f;
    }

    public Sprite(String name, float scale) {
        this.name = name;
        this.scale = scale;
        this.rotation = 0f;
    }

    public Sprite(String name, float scale, float rotation) {
        this.name = name;
        this.scale = scale;
        this.rotation = rotation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getOriginX() {
        return originX;
    }

    public float getOriginY() {
        return originY;
    }

    public void setOrigin(float originX, float originY) {
        customOrigin = true;
        this.originX = originX;
        this.originY = originY;
    }

    public void unsetOrigin() {
        customOrigin = false;
    }

    public boolean isCustomOrigin() {
        return customOrigin;
    }
}
