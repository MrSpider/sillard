package de.passau.ieee.woc.sillard.model;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;

public class ModelConstants {

    public static enum Teams {
        TeamA, TeamB;
    }
    
    public static class MeeplePositions {
        public static final Vector2[][] INITIAL_POSITION = new Vector2[4][5];

        static {
            INITIAL_POSITION[0][0] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.5), (int) (FIELD_HEIGHT * 0.33));
            INITIAL_POSITION[0][1] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.5), (int) (FIELD_HEIGHT * 0.67));
            
            INITIAL_POSITION[1][0] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.33));
            INITIAL_POSITION[1][1] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.67));
            INITIAL_POSITION[1][2] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.67), (int) (FIELD_HEIGHT * 0.5));

            INITIAL_POSITION[2][0] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.33));
            INITIAL_POSITION[2][1] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.67));
            INITIAL_POSITION[2][2] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.67), (int) (FIELD_HEIGHT * 0.33));
            INITIAL_POSITION[2][3] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.67), (int) (FIELD_HEIGHT * 0.67));

            INITIAL_POSITION[3][0] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.25));
            INITIAL_POSITION[3][1] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.5));
            INITIAL_POSITION[3][2] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.33), (int) (FIELD_HEIGHT * 0.75));
            INITIAL_POSITION[3][3] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.67), (int) (FIELD_HEIGHT * 0.33));
            INITIAL_POSITION[3][4] = new Vector2((int) ((FIELD_WIDTH / 2) * 0.67), (int) (FIELD_HEIGHT * 0.67));
        }
    }



    public final static int FIELD_HEIGHT = 90;
    public final static int FIELD_WIDTH = 160;

    public final static int GOAL_SIZE = 30;
    public final static float PLAYER_RADIUS = 6f;

    public final static float MAX_VELOCITY = 250;
    public final static float MAX_USER_ARROW_LENGTH = 15;

    public final static short CATEGORY_ARROW = 0x0001;
    public final static short CATEGORY_OTHER = 0x0002;
    public final static short MASK_ARROW = 0;
    public final static short MASK_OTHER = -1;
    
    public final static int EDGE_LENGTH = 10;

    public static enum Groups {
        PLAYER, BALL, GOAL, WALL, ARROW;
    }

    public static class Shapes {
        public static final CircleShape PLAYER_SHAPE = new CircleShape();
        public static final CircleShape BALL_SHAPE = new CircleShape();
        public static final EdgeShape GOAL_SHAPE = new EdgeShape();
        public static final EdgeShape H_WALL_SHAPE = new EdgeShape();
        public static final EdgeShape V_WALL_SHAPE = new EdgeShape();

        static {
            PLAYER_SHAPE.setRadius(PLAYER_RADIUS);
            BALL_SHAPE.setRadius(3.5f);
            GOAL_SHAPE.set(0, 0, 0, GOAL_SIZE);
            H_WALL_SHAPE.set(0, 0, FIELD_WIDTH, 0);
            V_WALL_SHAPE.set(0, 0, 0, (FIELD_HEIGHT - GOAL_SIZE) / 2);
        }
    }

    public static class Fixtures {
        public static final FixtureDef PLAYER_FIXTURE = new FixtureDef();
        public static final FixtureDef BALL_FIXTURE = new FixtureDef();
        public static final FixtureDef GOAL_FIXTURE = new FixtureDef();
        public static final FixtureDef H_WALL_FIXTURE = new FixtureDef();
        public static final FixtureDef V_WALL_FIXTURE = new FixtureDef();

        static {
            PLAYER_FIXTURE.shape = Shapes.PLAYER_SHAPE;
            PLAYER_FIXTURE.friction = 0.9f;
            PLAYER_FIXTURE.restitution = 0.9f;
            PLAYER_FIXTURE.filter.categoryBits = CATEGORY_OTHER;
            PLAYER_FIXTURE.filter.maskBits = MASK_OTHER;
            BALL_FIXTURE.shape = Shapes.BALL_SHAPE;
            BALL_FIXTURE.friction = 0.9f;
            BALL_FIXTURE.restitution = 0.9f;
            BALL_FIXTURE.filter.categoryBits = CATEGORY_OTHER;
            BALL_FIXTURE.filter.maskBits = MASK_OTHER;
            GOAL_FIXTURE.shape = Shapes.GOAL_SHAPE;
            GOAL_FIXTURE.filter.categoryBits = CATEGORY_OTHER;
            GOAL_FIXTURE.filter.maskBits = MASK_OTHER;
            H_WALL_FIXTURE.shape = Shapes.H_WALL_SHAPE;
            H_WALL_FIXTURE.filter.categoryBits = CATEGORY_OTHER;
            H_WALL_FIXTURE.filter.maskBits = MASK_OTHER;
            H_WALL_FIXTURE.restitution = 1f;
            V_WALL_FIXTURE.shape = Shapes.V_WALL_SHAPE;
            V_WALL_FIXTURE.filter.categoryBits = CATEGORY_OTHER;
            V_WALL_FIXTURE.filter.maskBits = MASK_OTHER;
            V_WALL_FIXTURE.restitution = 1f;
        }
    }
}
