package de.passau.ieee.woc.sillard.model;

import com.artemis.Entity;
import com.artemis.World;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;

import de.passau.ieee.woc.sillard.model.components.AIInput;
import de.passau.ieee.woc.sillard.model.components.Arrow;
import de.passau.ieee.woc.sillard.model.components.Physics;
import de.passau.ieee.woc.sillard.model.components.PlayerInput;
import de.passau.ieee.woc.sillard.model.systems.PhysicsSystem;

public class EntityFactory {

    public static Entity createSoccerPlayer(World world, String player, float x, float y) {
        Entity e = world.createEntity();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(ModelConstants.Fixtures.PLAYER_FIXTURE);
        
        e.addComponent(new Physics(body));
        
        if (player.equals(ModelConstants.Teams.TeamA.toString())) {
            e.addComponent(new PlayerInput(body, createArrow(world, x, y)));
        } else {
            e.addComponent(new AIInput(body));
        }

        world.getManager(PlayerManager.class).setPlayer(e, player);
        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.PLAYER.toString());
        e.addToWorld();

        return e;
    }

    public static Entity createBall(World world, float x, float y) {
        Entity e = world.createEntity();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(ModelConstants.Fixtures.BALL_FIXTURE);

        e.addComponent(new Physics(body));
        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.BALL.toString());
        e.addToWorld();

        return e;
    }

    public static void createGoal(World world, String player, float x, float y) {
        Entity e = world.createEntity();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(x, y);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(ModelConstants.Fixtures.GOAL_FIXTURE);

        e.addComponent(new Physics(body));
        world.getManager(PlayerManager.class).setPlayer(e, player);
        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.GOAL.toString());
        e.addToWorld();
    }

    public static void createWall(World world, float x, float y, boolean big) {
        Entity e = world.createEntity();

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(x, y);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);

        if (big) {
            body.createFixture(ModelConstants.Fixtures.H_WALL_FIXTURE);
        } else {
            body.createFixture(ModelConstants.Fixtures.V_WALL_FIXTURE);
        }

        e.addComponent(new Physics(body));
        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.WALL.toString());
        e.addToWorld();
    }

    private static Entity createArrow(World world, float x, float y) {
        Entity e = world.createEntity();

        EdgeShape shape = new EdgeShape();
        FixtureDef fixture = new FixtureDef();
        fixture.shape = shape;
        fixture.friction = 0.9f;
        fixture.restitution = 0.9f;
        fixture.filter.categoryBits = ModelConstants.CATEGORY_ARROW;
        fixture.filter.maskBits = ModelConstants.MASK_ARROW;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(x, y);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(fixture);

        e.addComponent(new Arrow(fixture));
        e.addComponent(new Physics(body));

        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.ARROW.toString());
        e.addToWorld();
        return e;
    }

    public static Entity createBottomLeftCorner(SillardWorld world) {
        Entity e = world.createEntity();

        PolygonShape shape = new PolygonShape();
        FixtureDef fixture = new FixtureDef();
        shape.set(new Vector2[] {new Vector2(0, 0), new Vector2(ModelConstants.EDGE_LENGTH, 0), new Vector2(0, ModelConstants.EDGE_LENGTH)});
        fixture.shape = shape;
        fixture.friction = 0.9f;
        fixture.restitution = 0.9f;
        fixture.filter.categoryBits = ModelConstants.CATEGORY_OTHER;
        fixture.filter.maskBits = ModelConstants.MASK_OTHER;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(0, 0);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(fixture);

        e.addComponent(new Physics(body));

        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.WALL.toString());
        e.addToWorld();
        return e;
    }
    
    public static Entity createBottomRightCorner(SillardWorld world) {
        Entity e = world.createEntity();

        PolygonShape shape = new PolygonShape();
        FixtureDef fixture = new FixtureDef();
        shape.set(new Vector2[] {new Vector2(0, 0), new Vector2(ModelConstants.EDGE_LENGTH, 0), new Vector2(ModelConstants.EDGE_LENGTH, ModelConstants.EDGE_LENGTH)});
        fixture.shape = shape;
        fixture.friction = 0.9f;
        fixture.restitution = 0.9f;
        fixture.filter.categoryBits = ModelConstants.CATEGORY_OTHER;
        fixture.filter.maskBits = ModelConstants.MASK_OTHER;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(ModelConstants.FIELD_WIDTH - ModelConstants.EDGE_LENGTH, 0);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(fixture);

        e.addComponent(new Physics(body));

        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.WALL.toString());
        e.addToWorld();
        return e;
    }
    
    public static Entity createUpLeftCorner(SillardWorld world) {
        Entity e = world.createEntity();

        PolygonShape shape = new PolygonShape();
        FixtureDef fixture = new FixtureDef();
        shape.set(new Vector2[] {new Vector2(0, - ModelConstants.EDGE_LENGTH), new Vector2(ModelConstants.EDGE_LENGTH, 0), new Vector2(0, 0)});
        fixture.shape = shape;
        fixture.friction = 0.9f;
        fixture.restitution = 0.9f;
        fixture.filter.categoryBits = ModelConstants.CATEGORY_OTHER;
        fixture.filter.maskBits = ModelConstants.MASK_OTHER;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(0, ModelConstants.FIELD_HEIGHT);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(fixture);

        e.addComponent(new Physics(body));

        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.WALL.toString());
        e.addToWorld();
        return e;
    }
    
    public static Entity createUpRightCorner(SillardWorld world) {
        Entity e = world.createEntity();

        PolygonShape shape = new PolygonShape();
        FixtureDef fixture = new FixtureDef();
        shape.set(new Vector2[] {new Vector2(0, ModelConstants.EDGE_LENGTH), new Vector2(ModelConstants.EDGE_LENGTH, 0), new Vector2(ModelConstants.EDGE_LENGTH, ModelConstants.EDGE_LENGTH)});
        fixture.shape = shape;
        fixture.friction = 0.9f;
        fixture.restitution = 0.9f;
        fixture.filter.categoryBits = ModelConstants.CATEGORY_OTHER;
        fixture.filter.maskBits = ModelConstants.MASK_OTHER;

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyType.StaticBody;
        bodyDef.position.set(ModelConstants.FIELD_WIDTH - ModelConstants.EDGE_LENGTH, ModelConstants.FIELD_HEIGHT - ModelConstants.EDGE_LENGTH);
        Body body = world.getSystem(PhysicsSystem.class).getPhysicsWorld().createBody(bodyDef);
        body.createFixture(fixture);

        e.addComponent(new Physics(body));

        world.getManager(GroupManager.class).add(e, ModelConstants.Groups.WALL.toString());
        e.addToWorld();
        return e;
    }
}
