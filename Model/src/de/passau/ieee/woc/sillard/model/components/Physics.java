package de.passau.ieee.woc.sillard.model.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import de.passau.ieee.woc.sillard.model.ModelConstants;

public class Physics extends Component {
    Body body;

    public Physics(Body body) {
        this.body = body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public Body getBody() {
        return body;
    }

    public Vector2 getPosition() {
        return body.getPosition();
    }

    public void applyFriction(float dynamic, float constant) {
        Vector2 vec = body.getLinearVelocity().cpy();
        Vector2 dynV = vec.mul(1 - dynamic);

        // fast stop
        if(dynV.len() < ModelConstants.MAX_VELOCITY * 0.05f) dynV.mul(0.75f);

        Vector2 cons = body.getLinearVelocity().cpy().nor().mul(constant);
        body.setLinearVelocity(dynV.sub(cons));
    }
}
