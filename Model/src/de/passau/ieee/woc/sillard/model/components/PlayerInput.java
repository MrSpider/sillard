package de.passau.ieee.woc.sillard.model.components;

import com.artemis.Component;
import com.artemis.Entity;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import de.passau.ieee.woc.sillard.model.ModelConstants;

public class PlayerInput extends Component {

    private Body body;
    private Vector2 velocity;
    private Entity arrow;

    public PlayerInput(Body body, Entity arrow) {
        this.arrow = arrow;
        this.body = body;
    }

    public boolean isClicked(float x, float y) {
        float radius = ModelConstants.Shapes.PLAYER_SHAPE.getRadius();

        return body.getPosition().dst(x, y) < radius;
    }

    public void isReleased(float x, float y) {
        Vector2 pos = body.getPosition();
        float xv = pos.x - x;
        float yv = pos.y - y;
        float betr = (float) Math.sqrt(xv * xv + yv * yv);
        if (betr > ModelConstants.MAX_USER_ARROW_LENGTH) {
            xv *= (1 / betr) * ModelConstants.MAX_VELOCITY;
            yv *= (1 / betr) * ModelConstants.MAX_VELOCITY;
        } else {
            xv *= (1 / betr) * (ModelConstants.MAX_VELOCITY * (betr / ModelConstants.MAX_USER_ARROW_LENGTH));
            yv *= (1 / betr) * (ModelConstants.MAX_VELOCITY * (betr / ModelConstants.MAX_USER_ARROW_LENGTH));
        }
        velocity = new Vector2(xv, yv);
        //debug
        body.setLinearVelocity(velocity);
    }

    public Entity getArrow() {
        return arrow;
    }

    public Vector2 getVelocity() {
        if (velocity != null) {
            return velocity;
        } else {
            return new Vector2();
        }
    }

    public Vector2 calcDistVector(float x, float y) {
        Vector2 pos = body.getPosition();
        float xv = pos.x - x;
        float yv = pos.y - y;
        float betr = (float) Math.sqrt(xv * xv + yv * yv);
        if (betr > ModelConstants.MAX_USER_ARROW_LENGTH) {
            xv *= (1 / betr) * ModelConstants.MAX_USER_ARROW_LENGTH;
            yv *= (1 / betr) * ModelConstants.MAX_USER_ARROW_LENGTH;
        }
        return new Vector2(xv, yv);
    }
}
