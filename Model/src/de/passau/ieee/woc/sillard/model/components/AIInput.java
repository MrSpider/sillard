package de.passau.ieee.woc.sillard.model.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;

public class AIInput extends Component {

    private Body body;
    private Vector2 velocity;

    public AIInput(Body body) {
        this.body = body;
    }
    
    public Vector2 getVelocity() {
        if (velocity != null) {
            return velocity;
        } else {
            return new Vector2();
        }
    }

    public Body getBody() {
        return body;
    }
}
