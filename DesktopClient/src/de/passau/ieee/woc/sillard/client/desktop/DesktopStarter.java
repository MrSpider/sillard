package de.passau.ieee.woc.sillard.client.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;
import de.passau.ieee.woc.sillard.client.SillardGame;

public class DesktopStarter {
    public static void main(String[] args) {
        //TexturePacker2.process("assets/game", "assets/game", "game");
        //TexturePacker2.process("assets/menu", "assets/menu", "menu");

        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
        cfg.title = "Sillard";
        cfg.useGL20 = true;
        cfg.width = 1280;
        cfg.height = 720;
        //cfg.resizable = false;
        new LwjglApplication(new SillardGame(), cfg);
    }
}