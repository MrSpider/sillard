package de.passau.ieee.woc.sillard.android;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.surfaceview.FillResolutionStrategy;
import com.badlogic.gdx.backends.android.surfaceview.FixedResolutionStrategy;
import com.badlogic.gdx.backends.android.surfaceview.RatioResolutionStrategy;
import de.passau.ieee.woc.sillard.client.ClientConstants;
import de.passau.ieee.woc.sillard.client.SillardGame;

public class AndroidStarter extends AndroidApplication {
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useAccelerometer = false;
        cfg.useCompass = false;
        cfg.useWakelock = true;
        cfg.hideStatusBar = true;
        //cfg.resolutionStrategy = new FixedResolutionStrategy(ClientConstants.CAMERA_WIDTH, ClientConstants.CAMERA_HEIGHT);
        cfg.resolutionStrategy = new RatioResolutionStrategy(ClientConstants.CAMERA_WIDTH, ClientConstants.CAMERA_HEIGHT);
        //cfg.resolutionStrategy = new FillResolutionStrategy();
        cfg.useGL20 = true;
        initialize(new SillardGame(), cfg);
    }
}
