package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.systems.VoidEntitySystem;

import de.passau.ieee.woc.sillard.model.GoalListener;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.systems.PhysicsSystem;

public class HighscoreSystem extends VoidEntitySystem implements GoalListener {
    private final int scoreToWin;
    private int[] scores = new int[ModelConstants.Teams.values().length];

    public HighscoreSystem(int scoreToWin) {
        this.scoreToWin = scoreToWin;
    }

    public int[] getScores() {
        return scores;
    }

    @Override
    protected void initialize() {
        // set goal listener
        world.getSystem(PhysicsSystem.class).addGoalListener(this);
    }

    @Override
    protected void processSystem() {

    }

    @Override
    protected boolean checkProcessing() {
        return false;
    }

    @Override
    public void goal(String team) {
        // add score
        scores[ModelConstants.Teams.valueOf(team).ordinal()]++;

        // check won
        if (scores[ModelConstants.Teams.valueOf(team).ordinal()] >= scoreToWin) {
            world.getSystem(GameStateSystem.class).setState(GameStateSystem.State.GAMEOVER);
        } else {
            world.getSystem(GameStateSystem.class).setState(GameStateSystem.State.RESETTING);
        }
    }
}
