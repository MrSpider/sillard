package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.systems.VoidEntitySystem;
import de.passau.ieee.woc.sillard.model.systems.AISystem;
import de.passau.ieee.woc.sillard.model.systems.PhysicsSystem;

import java.util.LinkedList;
import java.util.List;

public class GameStateSystem extends VoidEntitySystem {
    public static final int INTERVAL = 1;
    private State previousState;
    private State state;
    private List<StateListener> stateListeners;

    public GameStateSystem() {
        super();
        stateListeners = new LinkedList<>();
    }

    public GameStateSystem(State state) {
        this();
        this.state = state;
    }

    public boolean addStateListener(StateListener stateListener) {
        return stateListeners.add(stateListener);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void stateChanged(State state) {
        switch (state) {
            case PLANNING:
                world.getSystem(PhysicsSystem.class).setPassive(true);
                break;
            case ANIMATING:
                world.getSystem(AISystem.class).setPassive(false);
                world.getSystem(PhysicsSystem.class).setPassive(false);
                // the system is alive, it just doesn't know it yet
                world.getSystem(PhysicsSystem.class).setSystemAlive(true);
                break;
            case RESETTING:
                world.getSystem(GameStateSystem.class).setState(GameStateSystem.State.PLANNING);
                break;
            default:
                break;
        }
    }

    public enum State {
        PLANNING, ANIMATING, RESETTING, GOAL, GAMEOVER
    }

    public interface StateListener {
        void stateChanged(State state);
    }

    @Override
    protected void processSystem() {
        if (previousState != state) {
            previousState = state;

            // our own state handling
            stateChanged(previousState);

            // everyone else who likes states
            for (StateListener listener : stateListeners) {
                listener.stateChanged(previousState);
            }
        }

        // Check if we are finished animating
        if (state == State.ANIMATING) {
            if (!world.getSystem(PhysicsSystem.class).isSystemAlive()) {
                state = State.PLANNING;
            }
        }
    }
}
