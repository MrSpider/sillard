package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.managers.GroupManager;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import de.passau.ieee.woc.sillard.client.InputHelper;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.components.Physics;

public class SpriteRenderSystem extends EntityProcessingSystem {
    @Mapper
    ComponentMapper<Physics> pm;
    @Mapper
    ComponentMapper<de.passau.ieee.woc.sillard.model.components.Sprite> sm;
    private Camera camera;
    private TextureAtlas textureAtlas;
    private SpriteBatch spriteBatch;

    @SuppressWarnings("unchecked")
    public SpriteRenderSystem(Camera camera, TextureAtlas textureAtlas) {
        super(Aspect.getAspectForAll(Physics.class, de.passau.ieee.woc.sillard.model.components.Sprite.class));
        this.camera = camera;
        this.spriteBatch = new SpriteBatch();
        this.textureAtlas = textureAtlas;
    }

    @Override
    protected void begin() {
        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();
    }

    @Override
    protected void end() {
        spriteBatch.end();
    }

    @Override
    protected void process(Entity entity) {
        Vector2 translated = InputHelper.translateBoxToWorld(pm.get(entity).getPosition());
        TextureAtlas.AtlasRegion region = textureAtlas.findRegion(sm.get(entity).getName());
        Vector2 origin = sm.get(entity).isCustomOrigin() ? new Vector2(sm.get(entity).getOriginX(), sm.get(entity).getOriginY())
                : new Vector2(region.getRegionWidth() / 2, region.getRegionHeight() / 2);
        
        float rotation;
        
        // Check if the current entity is a ball.
        // Yes, it really is _that_ simple.
        if (world.getManager(GroupManager.class).getGroups(entity).contains(ModelConstants.Groups.BALL.toString())) {
            // rotate the ball depending on its x-coordinate
            rotation = (translated.x / 201 * 360) % 360;
        } else {
            rotation = sm.get(entity).getRotation();
        }
        spriteBatch.draw(
                region,
                translated.x - region.getRegionWidth() / 2, translated.y - region.getRegionHeight() / 2,
                origin.x, origin.y, // origin
                region.getRegionWidth(), region.getRegionHeight(),
                sm.get(entity).getScale(), sm.get(entity).getScale(), // scale
                rotation);
    }
}
