package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.Aspect;
import com.artemis.Entity;
import com.artemis.managers.GroupManager;
import com.artemis.managers.PlayerManager;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import de.passau.ieee.woc.sillard.client.ClientConstants;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.components.Physics;
import de.passau.ieee.woc.sillard.model.components.Sprite;

public class SpriteSetupSystem extends EntityProcessingSystem {
    private TextureAtlas atlas;

    @SuppressWarnings("unchecked")
    public SpriteSetupSystem(TextureAtlas atlas) {
        super(Aspect.getAspectForAll(Physics.class));
        this.atlas = atlas;
    }

    @Override
    protected void process(Entity entity) {
    }

    @Override
    protected boolean checkProcessing() {
        // we dont process here
        return false;
    }

    @Override
    protected void inserted(Entity e) {
        ImmutableBag<String> groups = world.getManager(GroupManager.class).getGroups(e);
        String players = world.getManager(PlayerManager.class).getPlayer(e);

        // All players get sprites
        if (groups.contains(ModelConstants.Groups.PLAYER.toString())) {
            float playerScale = (ModelConstants.Shapes.PLAYER_SHAPE.getRadius() * ClientConstants.BOX_TO_WORLD * 2) / atlas.findRegion("player1").getRegionWidth();
            if (players.contains(ModelConstants.Teams.TeamA.toString())) {
                e.addComponent(new Sprite("player1", playerScale));
            } else {
                e.addComponent(new Sprite("player2", playerScale));
            }
            world.changedEntity(e);
        } else if (groups.contains(ModelConstants.Groups.GOAL.toString())) {
            /*
            if (players.contains(ModelConstants.Teams.TeamA.toString())) {
                e.addComponent(new Sprite("goal"));
                e.getComponent(Sprite.class).setOrigin(32, 96);
            } else {
                e.addComponent(new Sprite("goal", 1f, 180));
                e.getComponent(Sprite.class).setOrigin(16, 96);
            }
            world.changedEntity(e);
            */
        } else if (groups.contains(ModelConstants.Groups.BALL.toString())) {
            float ballScale = (ModelConstants.Shapes.BALL_SHAPE.getRadius() * ClientConstants.BOX_TO_WORLD * 2) / atlas.findRegion("ball").getRegionWidth();
            e.addComponent(new Sprite("ball", ballScale));
            world.changedEntity(e);
        }
    }
}
