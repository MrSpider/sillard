package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import de.passau.ieee.woc.sillard.client.ClientConstants;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.systems.PhysicsSystem;

public class DebugRendererSystem extends VoidEntitySystem {
    private SpriteBatch spriteBatch;
    private Label fpsLabel;
    private Matrix4 projMatrix;
    private Box2DDebugRenderer renderer;

    public DebugRendererSystem(Camera camera) {
        spriteBatch = new SpriteBatch();
        spriteBatch.setProjectionMatrix(camera.combined);

        fpsLabel = new Label("", new Label.LabelStyle(new BitmapFont(), Color.BLACK));
        fpsLabel.setPosition(ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD, ModelConstants.FIELD_HEIGHT* ClientConstants.BOX_TO_WORLD);

        this.projMatrix = new Matrix4(camera.combined).scale(ClientConstants.BOX_TO_WORLD, ClientConstants.BOX_TO_WORLD, 0);
        this.renderer = new Box2DDebugRenderer();
    }

    @Override
    protected void processSystem() {
        spriteBatch.begin();
        fpsLabel.setText(String.valueOf(Gdx.graphics.getFramesPerSecond()));
        fpsLabel.draw(spriteBatch, 1f);
        spriteBatch.end();

        renderer.render(world.getSystem(PhysicsSystem.class).getPhysicsWorld(), projMatrix);
    }
}
