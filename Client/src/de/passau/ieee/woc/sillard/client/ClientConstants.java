package de.passau.ieee.woc.sillard.client;

public class ClientConstants {
    // the camera size is fixed and independent of the window size
    public final static int CAMERA_WIDTH = 1280;
    public final static int CAMERA_HEIGHT = 720;
    public final static float CAMERA_ASPECT = CAMERA_WIDTH / CAMERA_HEIGHT;

    // apparently determined by trial and error
    public final static float BOX_TO_WORLD = 6f;
    public final static float WORLD_TO_BOX = 1f / BOX_TO_WORLD;

    // difference between the size of the field and the camera size
    public final static int PADDING_WIDTH = (int) ((ClientConstants.CAMERA_WIDTH - (de.passau.ieee.woc.sillard.model.ModelConstants.FIELD_WIDTH * BOX_TO_WORLD)) / 2f);
    public final static int PADDING_HEIGHT = (int) ((ClientConstants.CAMERA_HEIGHT - (de.passau.ieee.woc.sillard.model.ModelConstants.FIELD_HEIGHT * BOX_TO_WORLD)) / 2f);
}
