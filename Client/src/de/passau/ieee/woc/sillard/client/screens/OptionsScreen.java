package de.passau.ieee.woc.sillard.client.screens;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import de.passau.ieee.woc.sillard.client.SillardGame;

public class OptionsScreen extends MenuScreen {
    private int meepleCount = 3;
    private int scoreToWin = 5;
    
    private TextButton meepleCountButton;
    private TextButton scoreToWinButton;

    private ChangeListener localChangeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            // networkMode = false;
            // updateButtonImages();
        }
    };
    

    private ChangeListener networkChangeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            // networkMode = true;
            // updateButtonImages();
        }
    };

    private ChangeListener beginGameChangeListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            // start the game by switching to the GameScreen
            parentGame.setScreen(new GameScreen(parentGame, meepleCount, scoreToWin));
        }
    };

    private ChangeListener meepleUpButtonListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            meepleCount = Math.min(meepleCount + 1, 5);
            meepleCountButton.setText(meepleCount + " meeples per player");
        }
    };

    private ChangeListener meepleDownButtonListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            meepleCount = Math.max(meepleCount - 1, 2);
            meepleCountButton.setText(meepleCount + " meeples per player");
        }
    };

    private ChangeListener scoreUpButtonListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            scoreToWin = scoreToWin + 1;
            scoreToWinButton.setText(scoreToWin + " goals to win");
        }
    };

    private ChangeListener scoreDownButtonListener = new ChangeListener() {
        @Override
        public void changed(ChangeEvent event, Actor actor) {
            scoreToWin = Math.max(scoreToWin - 1, 1);
            if (scoreToWin == 1) scoreToWinButton.setText(scoreToWin + " goal to win");
            else scoreToWinButton.setText(scoreToWin + " goals to win");
        }
    };


    public OptionsScreen(final SillardGame parentGame) {
        super(parentGame);

        final Table table = new Table();
        table.defaults().center();

        TextButtonStyle buttonStyle = new TextButtonStyle();
        buttonStyle.font = generator.generateFont(30);
        NinePatchDrawable patch = new NinePatchDrawable(new NinePatch(this.p));
        buttonStyle.up = patch;
        buttonStyle.down = patch;
        buttonStyle.over = patch;

        addLabel(table, 4, "Options");
        table.row();

        // TODO add again when network is working
        //addTextButton(table, 2, "Local", localChangeListener);
        //addTextButton(table, 2, "Network", networkChangeListener);
        table.row();

        addTextButton(table, 1, "-", meepleDownButtonListener);

        meepleCountButton = new TextButton("3 meeples per player", buttonStyle);
        table.add(meepleCountButton).center().colspan(2).space(20);
        addTextButton(table, 1, "+", meepleUpButtonListener);
        table.row();

        addTextButton(table, 1, "-", scoreDownButtonListener);
        scoreToWinButton = new TextButton("5 goals to win", buttonStyle);
        table.add(scoreToWinButton).center().colspan(2).space(20);
        addTextButton(table, 1, "+", scoreUpButtonListener);
        table.row();

        addTextButton(table, 4, "Start Game", beginGameChangeListener);

        stage.addActor(table);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
    }
}
