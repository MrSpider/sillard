package de.passau.ieee.woc.sillard.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import de.passau.ieee.woc.sillard.client.ClientConstants;
import de.passau.ieee.woc.sillard.client.SillardGame;
import de.passau.ieee.woc.sillard.client.UserInputProcessor;
import de.passau.ieee.woc.sillard.client.systems.*;
import de.passau.ieee.woc.sillard.model.SillardWorld;

public class GameScreen implements Screen, GameStateSystem.StateListener {
    private final SillardGame parentGame;
    private final SillardWorld world;
    private final InputMultiplexer inputMultiplexer;
    private final Camera camera;
    private final ShapeRenderer shapeRenderer;
    private final int meepleCount;
    private UserInputProcessor userInputProcessor;
    private HUDRenderSystem hud;

    public GameScreen(SillardGame game, int meepleCount, int scoreToWin) {
        this.parentGame = game;
        this.meepleCount = meepleCount;

        // create camera
        camera = new OrthographicCamera(ClientConstants.CAMERA_WIDTH, ClientConstants.CAMERA_HEIGHT);
        camera.position.set(
                ClientConstants.CAMERA_WIDTH / 2 - ClientConstants.PADDING_WIDTH,
                ClientConstants.CAMERA_HEIGHT / 2 - ClientConstants.PADDING_HEIGHT, 0);
        camera.update();


        inputMultiplexer = new InputMultiplexer();
        Gdx.input.setInputProcessor(inputMultiplexer);

        shapeRenderer = new ShapeRenderer();
        final TextureAtlas textureAtlas = game.getAssetManager().get("assets/game/game.atlas", TextureAtlas.class);

        hud = new HUDRenderSystem(parentGame, camera, inputMultiplexer,
                new ChangeListener() {
            @Override
            public void changed(ChangeEvent arg0, Actor arg1) {
                world.getSystem(GameStateSystem.class).setState(GameStateSystem.State.ANIMATING);
            }
        }, game.getAssetManager());

        world = new SillardWorld();
        world.setSystem(new GameStateSystem(GameStateSystem.State.PLANNING));
        world.setSystem(new SpriteSetupSystem(textureAtlas));
        world.setSystem(new HighscoreSystem(scoreToWin));
//        world.setSystem(new DebugRendererSystem(camera));
        world.setSystem(new FieldRenderSystem(camera, game.getAssetManager()));
        world.setSystem(new SpriteRenderSystem(camera, textureAtlas));
        world.setSystem(new ArrowRenderSystem(camera, textureAtlas));
        world.setSystem(hud);

        // set state listener
        world.getSystem(GameStateSystem.class).addStateListener(this);

        // setup the world
        world.initialize();
        world.setupPlayers(this.meepleCount);

        // Set input systems
        userInputProcessor = new UserInputProcessor(world);
        inputMultiplexer.addProcessor(userInputProcessor);
    }

    @Override
    public void stateChanged(GameStateSystem.State state) {
        switch (state) {
            case PLANNING:
                hud.setGoButtonActive(false);

                if (!inputMultiplexer.getProcessors().contains(userInputProcessor, true))
                    inputMultiplexer.addProcessor(userInputProcessor);

                break;
            case ANIMATING:
                // no input while animating
                inputMultiplexer.removeProcessor(userInputProcessor);
                hud.setGoButtonActive(true);

                // we don't need those anymore
                world.resetArrows();

                break;
            case RESETTING:
                inputMultiplexer.addProcessor(userInputProcessor);

                // reset the players
                world.clearPlayers();
                world.setupPlayers(meepleCount);

                break;
            case GAMEOVER:
                parentGame.setScreen(new GameOverScreen(parentGame, world.getSystem(HighscoreSystem.class).getScores()));
                break;
            default:
                break;
        }
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        Gdx.gl.glClearColor(0.1f, .7f, 0.1f, 1.f);

        world.setDelta(delta);
        world.process();

        // Debug points
        shapeRenderer.setProjectionMatrix(camera.combined);

        shapeRenderer.setColor(1f, 0f, 0f, 1f);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
        shapeRenderer.point(0, 0, 0);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Point);
        shapeRenderer.point(10, 10, 0);
        shapeRenderer.end();
    }

    @Override
    public void resize(int width, int height) {
    }

    @Override
    public void show() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {
    }
}
