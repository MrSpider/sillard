package de.passau.ieee.woc.sillard.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import de.passau.ieee.woc.sillard.client.SillardGame;

public class LoadingScreen implements Screen {

    final private SillardGame parentGame;
    private Stage stage;

    public LoadingScreen(SillardGame parentGame) {
        this.parentGame = parentGame;

        stage = new Stage(0, 0, true);

        final Table table = new Table();

        table.defaults().center();

        LabelStyle labelStyle = new LabelStyle();
        labelStyle.font = new BitmapFont();
        Label loadingScreenLabel = new Label("loading...", labelStyle);
        table.add(loadingScreenLabel);

        table.row();

        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub

    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();

        if (parentGame.getAssetManager().update()) {
            // we're done loading, let's move to the menu screen
            parentGame.setScreen(new MainMenuScreen(parentGame));
        }
        // there's no sense in showing a progress bar, since we're only packing a single file
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(width, height, true);

        // select the first actor, which should be the main table
        Actor menu = stage.getActors().first();

        // center the first actor (main table)
        menu.setX(stage.getWidth() / 2 - menu.getWidth() / 2);
        menu.setY(stage.getHeight() / 2 - menu.getHeight() / 2);
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
    }

}
