package de.passau.ieee.woc.sillard.client;

import com.artemis.Entity;
import com.artemis.utils.ImmutableBag;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.SillardWorld;
import de.passau.ieee.woc.sillard.model.components.Arrow;
import de.passau.ieee.woc.sillard.model.components.Physics;
import de.passau.ieee.woc.sillard.model.components.PlayerInput;

public class UserInputProcessor implements InputProcessor {
    private SillardWorld world;
    private Entity touchedPlayer;
    private Entity touchedArrow;

    public UserInputProcessor(SillardWorld world) {
        this.world = world;
    }

    @Override
    public boolean keyDown(int arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyTyped(char arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean keyUp(int arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean mouseMoved(int arg0, int arg1) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean scrolled(int arg0) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        // TODO: size up the hitbox for easier targeting on android
        if(button != Input.Buttons.LEFT) return false;

        Vector2 translated = InputHelper.translateInputToBox(new Vector2(screenX, screenY));
        ImmutableBag<Entity> ents = world.getPlayers();

        for (int i = 0; i < ents.size(); i++) {
            // we only look at players, which got clicked
            if (ents.get(i).getComponent(PlayerInput.class) == null || !ents.get(i).getComponent(PlayerInput.class).isClicked(translated.x, translated.y)) {
                continue;
            }

            touchedPlayer = ents.get(i);
            touchedArrow = ents.get(i).getComponent(PlayerInput.class).getArrow();

            // set arrow position
            Body bdy = touchedArrow.getComponent(Physics.class).getBody();
            bdy.setTransform(ents.get(i).getComponent(Physics.class).getPosition(), bdy.getAngle());
            return true;
        }
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        if (touchedPlayer == null) return false;

        Vector2 translated = InputHelper.translateInputToBox(new Vector2(screenX, screenY));
        Body body = touchedArrow.getComponent(Physics.class).getBody();

        // destroy any fixtures the arrow has
        if (!body.getFixtureList().isEmpty()) body.destroyFixture(body.getFixtureList().get(0));

        if (body.getPosition().dst(translated) > ModelConstants.PLAYER_RADIUS) {
            FixtureDef fixtureDef = touchedArrow.getComponent(Arrow.class).getFixtureDef();

            // calculate new arrow length
            Vector2 newSize = touchedPlayer.getComponent(PlayerInput.class).calcDistVector(translated.x, translated.y).mul(-1);
            ((EdgeShape) fixtureDef.shape).set(new Vector2(), newSize);
            touchedArrow.getComponent(Arrow.class).setSize(newSize);

            // create a new fixture
            touchedArrow.getComponent(Physics.class).getBody().createFixture(fixtureDef);
        } else {
            FixtureDef fixtureDef = touchedArrow.getComponent(Arrow.class).getFixtureDef();
            ((EdgeShape) fixtureDef.shape).set(Vector2.Zero, Vector2.Zero);
            touchedArrow.getComponent(Arrow.class).setSize(Vector2.Zero);

            // create a new fixture
            touchedArrow.getComponent(Physics.class).getBody().createFixture(fixtureDef);
        }

        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        if (touchedPlayer == null || button != Input.Buttons.LEFT) return false;

        Vector2 translated = InputHelper.translateInputToBox(new Vector2(screenX, screenY));
        Body body = touchedArrow.getComponent(Physics.class).getBody();

        if (translated.dst(body.getPosition()) > ModelConstants.PLAYER_RADIUS) {
            touchedPlayer.getComponent(PlayerInput.class).isReleased(translated.x, translated.y);
        }

        touchedPlayer = null;

        // destroy any fixtures the arrow has
        if (!body.getFixtureList().isEmpty()) body.destroyFixture(body.getFixtureList().get(0));

        return true;
    }
}
