package de.passau.ieee.woc.sillard.client.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;

import de.passau.ieee.woc.sillard.client.SillardGame;

public class MainMenuScreen extends MenuScreen {
    public MainMenuScreen(final SillardGame parentGame) {
        super(parentGame);

        final Table table = new Table();

        table.defaults().center();
        
        addLabel(table, 2, "Sillard v0.42a");
        table.row();

        addTextButton(table, 2, "Play",
                new ChangeListener() {
                    @Override
                    public void changed(ChangeEvent event, Actor actor) {
                        // start the game by switching to the GameScreen
                        parentGame.setScreen(new OptionsScreen(parentGame));
                    }
                });
        table.row();

        addTextButton(table, 2, "Quit", getExitListener());
        table.row();

        stage.addActor(table);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void render(float delta) {
        super.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
    }
}