package de.passau.ieee.woc.sillard.client;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import de.passau.ieee.woc.sillard.client.screens.LoadingScreen;

public class SillardGame extends Game {
    private AssetManager assetManager;

    public SillardGame() {
        assetManager = new AssetManager();
        assetManager.load("assets/game/game.atlas", TextureAtlas.class);
        assetManager.load("assets/menu/menu.atlas", TextureAtlas.class);
        assetManager.load("assets/backgrounds/menu_bg.png", Texture.class);
        assetManager.load("assets/backgrounds/background.png", Texture.class);
    }

    public AssetManager getAssetManager() {
        return assetManager;
    }

    @Override
    public void create() {
        // TODO: asset loading while loading screen
        setScreen(new LoadingScreen(this));
    }
}
