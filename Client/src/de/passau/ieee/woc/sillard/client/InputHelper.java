package de.passau.ieee.woc.sillard.client;

import static de.passau.ieee.woc.sillard.client.ClientConstants.BOX_TO_WORLD;
import static de.passau.ieee.woc.sillard.client.ClientConstants.WORLD_TO_BOX;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class InputHelper {
    public static Vector2 translateInputToBox(Vector2 input) {
        // translate origin from top left to bottom left
        Vector2 result = new Vector2(input.x, Gdx.graphics.getHeight() - input.y);
        
        // translate window to camera
        result.mul(
                (float) ClientConstants.CAMERA_WIDTH / Gdx.graphics.getWidth(), 
                (float) ClientConstants.CAMERA_HEIGHT / Gdx.graphics.getHeight());
        
        // subtract the padding (difference between field size and window/camera size)
        result.sub(ClientConstants.PADDING_WIDTH, ClientConstants.PADDING_HEIGHT);
        result.mul(WORLD_TO_BOX, WORLD_TO_BOX);
        return result;
    }

    public static Vector2 translateBoxToWorld(Vector2 box) {
        Vector2 result = new Vector2(box);
        result.mul(BOX_TO_WORLD, BOX_TO_WORLD);
        return result;
    }

    public static Vector2 translateWorldToBox(Vector2 world) {
        Vector2 result = new Vector2(world);
        result.mul(WORLD_TO_BOX, WORLD_TO_BOX);
        return result;
    }
}
