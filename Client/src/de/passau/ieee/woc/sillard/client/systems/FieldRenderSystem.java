package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;

import de.passau.ieee.woc.sillard.client.ClientConstants;
import de.passau.ieee.woc.sillard.model.ModelConstants;

public class FieldRenderSystem extends VoidEntitySystem {
    ShapeRenderer shapeRenderer;
    Texture backgroundTexture;
    SpriteBatch batch;

    public FieldRenderSystem(Camera camera, AssetManager mgr) {
        shapeRenderer = new ShapeRenderer();
        shapeRenderer.setProjectionMatrix(camera.combined);
        backgroundTexture = mgr.get("assets/backgrounds/background.png");
        batch = new SpriteBatch();
        batch.setProjectionMatrix(camera.combined);
    }

    @Override
    protected void processSystem() {
        batch.begin();
        backgroundTexture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
        batch.draw(backgroundTexture, -ClientConstants.PADDING_WIDTH, -ClientConstants.PADDING_HEIGHT, ClientConstants.CAMERA_WIDTH, ClientConstants.CAMERA_HEIGHT, 0, 0, 4 ,4);
        batch.end();
        
        // outer line
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0, 0, ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD, ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();

        // draw edge lines
        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.line(ModelConstants.EDGE_LENGTH * ClientConstants.BOX_TO_WORLD, 0, 0, ModelConstants.EDGE_LENGTH * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();
        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.line((ModelConstants.FIELD_WIDTH-ModelConstants.EDGE_LENGTH) * ClientConstants.BOX_TO_WORLD, 0, ModelConstants.FIELD_WIDTH* ClientConstants.BOX_TO_WORLD, ModelConstants.EDGE_LENGTH * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();
        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.line(ModelConstants.EDGE_LENGTH * ClientConstants.BOX_TO_WORLD, ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD, 0, (ModelConstants.FIELD_HEIGHT - ModelConstants.EDGE_LENGTH) * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();
        shapeRenderer.begin(ShapeType.Line);
        shapeRenderer.line((ModelConstants.FIELD_WIDTH-ModelConstants.EDGE_LENGTH) * ClientConstants.BOX_TO_WORLD, ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD, ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD, (ModelConstants.FIELD_HEIGHT - ModelConstants.EDGE_LENGTH) * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();

        // left outer goal box
        float outerBoxHeight = ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD / 2;
        float outerBoxWidth = ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 5;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0, 0 + (ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD - outerBoxHeight) / 2, outerBoxWidth, outerBoxHeight);
        shapeRenderer.end();

        // left inner goal box
        float innerBoxHeight = ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD / 2.5f;
        float innerBoxWidth = ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 15;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0, 0 + (ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD - innerBoxHeight) / 2, innerBoxWidth, innerBoxHeight);
        shapeRenderer.end();

        //left goal
        float goalHeight = ModelConstants.GOAL_SIZE * ClientConstants.BOX_TO_WORLD;
        float goalWidth = ClientConstants.BOX_TO_WORLD * 4;
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0-goalWidth, 0+(ModelConstants.FIELD_HEIGHT- ModelConstants.GOAL_SIZE) / 2 * ClientConstants.BOX_TO_WORLD, goalWidth, goalHeight);
        shapeRenderer.end();

        for(int i = 1; i < 18; i++) {
            float y = 0
                      + (ModelConstants.FIELD_HEIGHT - ModelConstants.GOAL_SIZE) / 2 * ClientConstants.BOX_TO_WORLD + goalHeight / 18 *i;

            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.line(0-goalWidth, y, 0, y);
            shapeRenderer.end();
        }
        for(int i = 1; i < 3; i++) {
            float x = 0 - goalWidth / 3 *i;

          shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
          shapeRenderer.line(x, 0 + (ModelConstants.FIELD_HEIGHT - ModelConstants.GOAL_SIZE ) / 2 * ClientConstants.BOX_TO_WORLD, x, 0 + ModelConstants.FIELD_HEIGHT  / 2 * ClientConstants.BOX_TO_WORLD + goalHeight / 2);
          shapeRenderer.end();
        }

        // right outer goal box
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0 + ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD - outerBoxWidth, 0 + (ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD - outerBoxHeight) / 2, outerBoxWidth, outerBoxHeight);
        shapeRenderer.end();

        // right inner goal box
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0 + ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD - innerBoxWidth, 0 + (ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD - innerBoxHeight) / 2, innerBoxWidth, innerBoxHeight);
        shapeRenderer.end();

        // right goal
        shapeRenderer.begin(ShapeRenderer.ShapeType.Rectangle);
        shapeRenderer.rect(0+ModelConstants.FIELD_WIDTH*ClientConstants.BOX_TO_WORLD, 0+(ModelConstants.FIELD_HEIGHT- ModelConstants.GOAL_SIZE) / 2 * ClientConstants.BOX_TO_WORLD, goalWidth, goalHeight);
        shapeRenderer.end();
        for(int i = 1; i < 18; i++) {
            float y = 0
                      + (ModelConstants.FIELD_HEIGHT - ModelConstants.GOAL_SIZE) / 2 * ClientConstants.BOX_TO_WORLD + goalHeight / 18 *i;

            shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
            shapeRenderer.line(0+ ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD, y, 0 + ModelConstants.FIELD_WIDTH*ClientConstants.BOX_TO_WORLD + goalWidth, y);
            shapeRenderer.end();
        }
        for(int i = 1; i < 3; i++) {
            float x = 0 + ModelConstants.FIELD_WIDTH*ClientConstants.BOX_TO_WORLD + goalWidth / 3 *i;

          shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
          shapeRenderer.line(x, 0 + (ModelConstants.FIELD_HEIGHT - ModelConstants.GOAL_SIZE ) / 2 * ClientConstants.BOX_TO_WORLD, x, 0 + ModelConstants.FIELD_HEIGHT  / 2 * ClientConstants.BOX_TO_WORLD + goalHeight / 2);
          shapeRenderer.end();
        }


        // middle line
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.line(
                0 + ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 2, 0,
                0 + ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 2, 0 + ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();

        // middle circle
        shapeRenderer.begin(ShapeRenderer.ShapeType.Circle);
        shapeRenderer.circle(0 + ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 2, 0 + ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD / 2, ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 14);
        shapeRenderer.end();

        // middle point
        shapeRenderer.begin(ShapeRenderer.ShapeType.FilledCircle);
        shapeRenderer.filledCircle(0 + ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD / 2, 0 + ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD / 2, 1 * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();

        // left point
        shapeRenderer.begin(ShapeRenderer.ShapeType.FilledCircle);
        shapeRenderer.filledCircle(goalWidth * 6, 0 + ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD / 2, 1 * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();

        // right point
        shapeRenderer.begin(ShapeRenderer.ShapeType.FilledCircle);
        shapeRenderer.filledCircle(ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD  - goalWidth * 6, 0 + ModelConstants.FIELD_HEIGHT * ClientConstants.BOX_TO_WORLD / 2, 1 * ClientConstants.BOX_TO_WORLD);
        shapeRenderer.end();
    }
}
