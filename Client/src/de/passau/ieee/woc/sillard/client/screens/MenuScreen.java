package de.passau.ieee.woc.sillard.client.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import de.passau.ieee.woc.sillard.client.SillardGame;

public abstract class MenuScreen implements Screen {

    private Texture backgroundTexture;
    private TextButtonStyle textStyle;
    private LabelStyle lableStyle;
    protected Stage stage;
    protected TextureAtlas textureAtlas;
    protected SillardGame parentGame;
    protected FreeTypeFontGenerator generator; 
    protected NinePatch p;

    public TextButtonStyle getTextStyle() {
        return textStyle;
    }

    public LabelStyle getLableStyle() {
        return lableStyle;
    }

    public MenuScreen(final SillardGame parentGame) {
        this.parentGame = parentGame;
        backgroundTexture = parentGame.getAssetManager().get("assets/backgrounds/background.png", Texture.class);
        backgroundTexture.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
        stage = new Stage(0, 0, true);
        textureAtlas = parentGame.getAssetManager().get("assets/menu/menu.atlas", TextureAtlas.class);
        Gdx.input.setInputProcessor(stage);
        generator = new FreeTypeFontGenerator(Gdx.files.internal("assets/fonts/rainmaker.otf"));
        p = new NinePatch((Texture) parentGame.getAssetManager().get("assets/backgrounds/menu_bg.png"), 12, 12, 12, 12);

        generateStyles();
    }

    protected ChangeListener getExitListener() {
       return new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                // blank screen until the program is terminated
                stage.clear();

                // Die. Immediately. Without cleaning anything up.
                // TODO: Clean up before terminating.
                Gdx.app.exit();
            }
        };
    }

    /**
    * @param table
    * @param imageIdentifier
    * @param hoverImageIdentifier
    * @param changeListener
    * @throws NullPointerException when image identifiers are invalid
    */
    protected void addTextButton(Table table, int colspan, String text, ChangeListener changeListener) {
       final TextButton button = new TextButton(text, textStyle);
       button.addListener(changeListener);
       table.add(button).center().colspan(colspan).space(20);
    }

    protected void addLabel(Table table, int colspan, String text) {
        final Label label= new Label(text, lableStyle);
        table.add(label).center().colspan(colspan).space(20);
    }

    public void generateStyles() {
        NinePatch p = new NinePatch(this.p);
        p.setColor(new Color(0, 0, 0, 0.5f));
        NinePatchDrawable patch = new NinePatchDrawable(p);

        p = new NinePatch(p);
        p.setColor(new Color(0, 0, 0, 1f));
        NinePatchDrawable patchDown = new NinePatchDrawable(p);

        textStyle = new TextButtonStyle();
        textStyle.up = patch;
        textStyle.down = patchDown;
        textStyle.over = patchDown;
        textStyle.font = generator.generateFont(48);
        textStyle.fontColor = Color.WHITE;
        
        lableStyle = new LabelStyle();
        lableStyle.background = patchDown;
        lableStyle.font = generator.generateFont(48);
        lableStyle.fontColor = Color.WHITE;  
    }
    
    @Override
    public void show() {
        // TODO Auto-generated method stub
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        // draw background
        stage.getSpriteBatch().begin();
        stage.getSpriteBatch().draw(backgroundTexture, 0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), 0, 0, 4, 4);
        stage.getSpriteBatch().end();
        
        stage.act(delta);
        stage.draw();
        Table.drawDebug(stage);
    }

    @Override
    public void resize(int width, int height) {
        stage.setViewport(width, height, true);

        // select the first actor, which should be the main table
        Actor menu = stage.getActors().first();

        if (menu != null) {
            // center the first actor (main table)
            menu.setX(stage.getWidth() / 2 - menu.getWidth() / 2);
            menu.setY(stage.getHeight() / 2 - menu.getHeight() / 2);
        }

    }

    @Override
    public void hide() {
        // TODO Auto-generated method stub
    }

    @Override
    public void pause() {
        // TODO Auto-generated method stub
    }

    @Override
    public void resume() {
        // TODO Auto-generated method stub
    }

    @Override
    public void dispose() {
        // TODO Auto-generated method stub
    }
}