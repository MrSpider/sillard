package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.systems.VoidEntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import de.passau.ieee.woc.sillard.client.ClientConstants;
import de.passau.ieee.woc.sillard.client.SillardGame;
import de.passau.ieee.woc.sillard.client.screens.MainMenuScreen;
import de.passau.ieee.woc.sillard.model.ModelConstants;

/**
 * Renders all buttons and scores on the display
 */
public class HUDRenderSystem extends VoidEntitySystem implements GameStateSystem.StateListener {
    private Stage stage;
    private final TextButton goButton;
    private final TextButton backButton;
    private final Label highscoreLabel;
    private TextButtonStyle textStyle;
    private NinePatchDrawable patchLight;
    private NinePatchDrawable patchDark;

    public HUDRenderSystem(final SillardGame parentGame, Camera camera, InputMultiplexer inputMultiplexer,
            ChangeListener goButtonClickListener, AssetManager mgr) {
        stage = new Stage();
        stage.setCamera(camera);

        Table table = new Table();
        table.defaults();
        table.debug();
        stage.addActor(table);

        NinePatch pLight = new NinePatch(new NinePatch((Texture) mgr.get("assets/backgrounds/menu_bg.png"), 12, 12, 12, 12));
        NinePatch pDark = new NinePatch(pLight);
        pLight.setColor(new Color(0, 0, 0, 0.5f));


        // Highscore label
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("assets/fonts/rainmaker.otf"));

        LabelStyle labelStyle = new LabelStyle();
        patchLight = new NinePatchDrawable(pLight);
        patchDark = new NinePatchDrawable(pDark);

        labelStyle.background = patchDark;
        labelStyle.font = generator.generateFont(48);
        labelStyle.fontColor = Color.WHITE;

        highscoreLabel = new Label("0 : 0", labelStyle);

        textStyle = new TextButtonStyle();
        textStyle.up = patchLight;
        textStyle.down = patchDark;
        textStyle.over = patchDark;
        textStyle.font = generator.generateFont(48);
        textStyle.fontColor = Color.WHITE;
        
        TextButtonStyle backStyle = new TextButtonStyle(textStyle);
        backStyle.up = patchLight;
        backStyle.down = patchDark;
        backStyle.over = patchDark;
        backButton = new TextButton("Menu", backStyle);
        backButton.addListener(new ChangeListener() {
            
            @Override
            public void changed(ChangeEvent arg0, Actor arg1) {
                    parentGame.setScreen(new  MainMenuScreen(parentGame));
            }
        });
        backButton.setPosition(0, -backButton.getHeight() - 5);

        goButton = new TextButton("next Step", textStyle);
        goButton.addListener(goButtonClickListener);
        goButton.setPosition(ModelConstants.FIELD_WIDTH * ClientConstants.BOX_TO_WORLD - goButton.getWidth(), -goButton.getHeight() - 5);

        stage.addActor(goButton);
        stage.addActor(backButton);
        inputMultiplexer.addProcessor(stage);
    }

    public void setGoButtonActive(boolean active) {
        textStyle.up = active ? patchDark : patchLight;
    }

    @Override
    protected void initialize() {
        world.getSystem(GameStateSystem.class).addStateListener(this);
    }

    @Override
    protected void processSystem() {
        stage.getSpriteBatch().begin();


        highscoreLabel.setText(world.getSystem(HighscoreSystem.class).getScores()[ModelConstants.Teams.TeamB.ordinal()] + " : " + world.getSystem(HighscoreSystem.class).getScores()[ModelConstants.Teams.TeamA.ordinal()]);
        highscoreLabel.setPosition(ClientConstants.CAMERA_WIDTH / 2 - highscoreLabel.getWidth() / 2 - ClientConstants.PADDING_WIDTH, ClientConstants.CAMERA_HEIGHT - ClientConstants.PADDING_HEIGHT * 2 + 5);

        highscoreLabel.draw(stage.getSpriteBatch(), 1);
        stage.getSpriteBatch().end();

        stage.act(world.getDelta());
        stage.draw();
        Table.drawDebug(stage);
    }

    @Override
    public void stateChanged(GameStateSystem.State state) {
        if(state == GameStateSystem.State.ANIMATING) {
            goButton.setDisabled(true);
        } else {
            goButton.setDisabled(false);
        }
    }
}
