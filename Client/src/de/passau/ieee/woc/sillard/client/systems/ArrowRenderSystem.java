package de.passau.ieee.woc.sillard.client.systems;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.annotations.Mapper;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;

import de.passau.ieee.woc.sillard.client.InputHelper;
import de.passau.ieee.woc.sillard.model.ModelConstants;
import de.passau.ieee.woc.sillard.model.components.Arrow;
import de.passau.ieee.woc.sillard.model.components.Physics;

public class ArrowRenderSystem extends EntityProcessingSystem {
    @Mapper
    ComponentMapper<Physics> pm;
    @Mapper
    ComponentMapper<Arrow> am;
    private Camera camera;
    private TextureAtlas textureAtlas;
    private SpriteBatch spriteBatch;

    @SuppressWarnings("unchecked")
    public ArrowRenderSystem(Camera camera, TextureAtlas textureAtlas) {
        super(Aspect.getAspectForAll(Arrow.class, Physics.class));
        this.textureAtlas = textureAtlas;
        this.camera = camera;
        this.spriteBatch = new SpriteBatch();
    }

    @Override
    protected void begin() {
        spriteBatch.setProjectionMatrix(camera.combined);
        spriteBatch.begin();
    }

    @Override
    protected void end() {
        spriteBatch.end();
    }

    @Override
    protected void process(Entity entity) {
        Vector2 arrowScale = entity.getComponent(Arrow.class).getSize();

        if (arrowScale.len() > ModelConstants.PLAYER_RADIUS) {
            Vector2 translated = InputHelper.translateBoxToWorld(pm.get(entity).getPosition());
            TextureAtlas.AtlasRegion region = textureAtlas.findRegion("arrow");

            /* Reduce arrow size in relation to the other graphics.
             * (by an arbitrary factor, dependent on the size of the arrow image)
             * We need to work on a copy of arrowScale as multiplying it would
             * change its value.
             */
            Vector2 scaled = arrowScale.cpy();
            scaled.mul(0.2f / ModelConstants.MAX_USER_ARROW_LENGTH);

            spriteBatch.draw(
                    region,
                    translated.x - region.getRegionWidth() / 2, translated.y,
                    region.getRegionWidth() / 2, 0, // origin
                    region.getRegionWidth(), region.getRegionHeight(),
                    scaled.len(), scaled.len(), // scale
                    arrowScale.angle() - 90);
        }
    }
}
