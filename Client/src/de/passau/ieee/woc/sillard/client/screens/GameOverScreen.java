package de.passau.ieee.woc.sillard.client.screens;

import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import de.passau.ieee.woc.sillard.client.SillardGame;
import de.passau.ieee.woc.sillard.model.ModelConstants;


public class GameOverScreen extends MenuScreen {

    public GameOverScreen(final SillardGame parentGame, final int[] score) {
        super(parentGame);
        
        final Table table = new Table();
        table.defaults().center();

        if (score[0] > score[1]) addLabel(table, 4, "Game Over");
        else addLabel(table, 4, "You Won");
        
        table.row();
        
        TextButtonStyle buttonStyle = new TextButtonStyle();
        buttonStyle.font = generator.generateFont(30);
        NinePatchDrawable patch = new NinePatchDrawable(new NinePatch(this.p));
        buttonStyle.up = patch;
        buttonStyle.down = patch;
        buttonStyle.over = patch;

        TextButton scoreDisplay = new TextButton("You: " + score[ModelConstants.Teams.TeamB.ordinal()] + " - Opponent: " + score[ModelConstants.Teams.TeamA.ordinal()], buttonStyle);
        scoreDisplay.setStyle(buttonStyle);
        table.add(scoreDisplay).center().colspan(4).space(20);
        
        table.row();
        
        addTextButton(table, 4, "Play Again", new ChangeListener() {
            @Override
            public void changed(ChangeEvent event, Actor actor) {
                // start the game by switching to the GameScreen
                parentGame.setScreen(new OptionsScreen(parentGame));
            }
        });
        table.row();
        
        addTextButton(table, 4, "Quit", getExitListener());
        stage.addActor(table);
    }

    @Override
    public void dispose() {
    }

    @Override
    public void hide() {
    }

    @Override
    public void pause() {
    }

    @Override
    public void render(float delta) {
       super.render(delta);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void resume() {
    }

    @Override
    public void show() {
    }
}