package de.passau.ieee.woc.sillard.network;

import java.io.IOException;
import java.util.Collection;

import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;

import de.passau.ieee.woc.sillard.network.packages.InitPackage;
import de.passau.ieee.woc.sillard.network.packages.Package;

public class GameClient {

    String serverAdress;
    int port;
    Client client;
    Kryo kryo;
    Listener listener;
    
    public GameClient(String adress, int port) {
       serverAdress = adress;
       client = new Client();
       client.start();
       init();
    }
    
    private void init() {
        kryo = client.getKryo();
        kryo.register(Package.class);
        kryo.register(InitPackage.class);

        listener = new Listener() {
            @Override
            public void received(Connection link, Object load) {
                if (load instanceof Package) {
                    
                } else if (load instanceof InitPackage) {
                    setNewGame();
                }

                client.addListener(listener);
            }
        };
    }
    
    protected void setNewGame() {
        
    }

    public void connect() {
        try {
            client.connect(1000, serverAdress, port);
        } catch (IOException e) {
            System.err.println("somthing with teh network went south");
            e.printStackTrace();
        }
    }
    
    public void send(Collection<Vector2> velocitys) {
        client.sendTCP(new Package(velocitys));
    }
}
