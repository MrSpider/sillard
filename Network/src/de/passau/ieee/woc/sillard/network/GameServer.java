package de.passau.ieee.woc.sillard.network;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;

import de.passau.ieee.woc.sillard.network.packages.InitPackage;
import de.passau.ieee.woc.sillard.network.packages.Package;

public class GameServer {
    private int meepleCount;
    private int scoreToWin;

    private Server server;
    private Kryo kryo;
    private Listener listener;

    public GameServer() {
        server = new Server();
        kryo = new Kryo();
        server.start();
        
        init();
    }

    private void init() {
        kryo = server.getKryo();
        kryo.register(Package.class);
        kryo.register(InitPackage.class);

        listener = new Listener() {
            @Override
            public void received(Connection link, Object load) {
                if (load instanceof Package) {

                } else if (load instanceof InitPackage) {
                    // Should no happen
                }

                server.addListener(listener);
            }
            
            @Override
            public void connected(Connection link) {
                link.sendTCP(new InitPackage(meepleCount, scoreToWin));
            }
        };
    }

    public void setNewGame(int meepleCount, int scoreToWin){
        this.meepleCount = meepleCount;
        this.scoreToWin = scoreToWin;
    }
}