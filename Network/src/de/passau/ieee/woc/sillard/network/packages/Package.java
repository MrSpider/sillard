package de.passau.ieee.woc.sillard.network.packages;

import java.util.Collection;

import com.badlogic.gdx.math.Vector2;

public class Package {
    
    public Collection<Vector2> velocitys;
    
    public Package() {
    }

    public Package(Collection<Vector2> velocitys) {
        this.velocitys = velocitys;
    }
}
