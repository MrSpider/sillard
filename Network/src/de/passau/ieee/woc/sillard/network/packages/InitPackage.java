package de.passau.ieee.woc.sillard.network.packages;

public class InitPackage {

    public int meebles;
    public int scoreToWin;

    public InitPackage() { }

    public InitPackage(int meebles, int scoreToWin) {
        this.meebles = meebles;
        this.scoreToWin = scoreToWin;
    }
}
